Source: sway
Section: x11
Priority: optional
Maintainer: Birger Schacht <birger@rantanplan.org>
Build-Depends:
 debhelper (>= 11),
 libwlroots-dev (>= 0.2),
 libwayland-dev,
 libinput-dev (>= 1.6.0),
 libcap-dev,
 libpcre3-dev,
 libjson-c-dev (>= 0.13),
 libpango1.0-dev,
 libcairo2-dev,
 libgdk-pixbuf2.0-dev,
 libpam0g-dev,
 libdbus-1-dev (>= 1.10),
 meson,
 pkgconf,
 tree,
 scdoc
Standards-Version: 4.2.1
Homepage: http://swaywm.org
Vcs-Browser: https://salsa.debian.org/swaywm-team/sway
Vcs-Git: https://salsa.debian.org/swaywm-team/sway.git
Rules-Requires-Root: no

Package: sway
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends}
Recommends: swaylock, sway-backgrounds
#Suggests: swaygrab
Description: i3-compatible Wayland compositor
 sway (SirCmpwn's Wayland compositor) is a tiling Wayland compositor and a
 drop-in replacement for the i3 window manager for X11. It works with your
 existing i3 configuration and supports most of i3's features, plus a few
 extras. This means it is a minimalist, tiling window manager.

Package: swaylock
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 sway (= ${binary:Version})
Description: Screen locker for the sway window manager.
 sway (SirCmpwn's Wayland compositor) is a tiling Wayland compositor and a
 drop-in replacement for the i3 window manager for X11. It works with your
 existing i3 configuration and supports most of i3's features, plus a few
 extras. This means it is a minimalist, tiling window manager.
 .
 swaylock is a screen locker for sway.

Package: sway-backgrounds
Architecture: all
Multi-Arch: foreign
Depends:
 ${shlibs:Depends},
 ${misc:Depends}
Suggests: sway
Description: Set of backgrounds packaged with the sway window manager
 sway (SirCmpwn's Wayland compositor) is a tiling Wayland compositor and a
 drop-in replacement for the i3 window manager for X11. It works with your
 existing i3 configuration and supports most of i3's features, plus a few
 extras. This means it is a minimalist, tiling window manager.
 .
 This package contains a set of desktop backgrounds that come with sway.
